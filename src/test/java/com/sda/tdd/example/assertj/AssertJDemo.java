package com.sda.tdd.example.assertj;

import org.junit.Test;
import java.util.HashMap;
import java.util.Map;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

public class AssertJDemo {

    @Test
    public void testAssertThatWithStringArray() {
        Integer[] arrays= {11,12,13,14};
        assertThat(arrays).hasSize(4).
                contains(12).doesNotContain(15);
    }


    @Test
    public void testAssertThatWithMap(){
        Map<Integer,String> map = new HashMap<Integer,String>();
        map.put(10,"ten");
        map.put(1,"one");
        assertThat(map)
                .isNotEmpty()
                .doesNotContainKeys(11)
                .containsKey(10)
                .contains(entry(1, "one"));
    }
}
