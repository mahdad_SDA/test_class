package com.sda.tdd.example.mockito.verify;

import calculator.Calculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.verification.NoInteractionsWanted;
import org.mockito.runners.MockitoJUnitRunner;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VerifyMockitoTest {


    @Mock
    Calculator calculator;


    @Test
    public void testAddVerify() {
        calculator.add(anyInt(),anyInt());
        verify(calculator).add(anyInt(), anyInt());
    }

    @Test
    public void testAddVerifyNTimes() {
        calculator.add(anyInt(),anyInt());
        calculator.add(anyInt(),anyInt());
        calculator.add(anyInt(),anyInt());
        verify(calculator, times(3)).
                add(anyInt(), anyInt());
    }

    @Test
    public void testVerifyWithList() {
        List<String> mockedList = mock(ArrayList.class);
        mockedList.size();
        verify(mockedList).size();
    }

    @Test
    public void testVerifyWithZeroInteractions() {
        List<String> mockedList = mock(ArrayList.class);
        verifyZeroInteractions(mockedList);
        verify(mockedList, times(0)).size();
    }


    @Test
    public void testVerifyAtLeastAtMostMethods() {
        List<String> mockedList = mock(ArrayList.class);
        mockedList.clear();
        mockedList.clear();
        mockedList.clear();

        verify(mockedList, atLeast(1)).clear();
        verify(mockedList, atMost(10)).clear();
    }

    @Test
    public void testVerifyNoInteraction() {
        List<String> mockedList = mock(ArrayList.class);
        mockedList.size();
        verify(mockedList, never()).clear();
    }


//    @Test(expected = NoInteractionsWanted.class)
    @Test
    public void testVerifyUnexpectedInteractions() {
        List<String> mockedList = mock(ArrayList.class);
        mockedList.size();
        mockedList.clear();
        verify(mockedList).size();
        verifyNoMoreInteractions(mockedList);
    }

}
