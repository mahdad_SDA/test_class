package com.sda.tdd.example.mockito.spy;

import calculator.Calculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SpyMockitoTest {

    @Test
    public void testSpy() {
        Calculator calculator = new Calculator();
        Calculator spyCalculator = spy(calculator);
        spyCalculator.add(4, 6);
        spyCalculator.add(3, 4);
        Mockito.verify(spyCalculator).add(4, 6);
        Mockito.verify(spyCalculator).add(3, 4);
//        Mockito.verify(spyCalculator).add(3, 3);
    }
    @Test
    public void testListWithMock() {
        List mockedList = Mockito.mock(ArrayList.class);

        mockedList.add("one");

        Mockito.verify(mockedList).add("one");

        assertEquals(0, mockedList.size());
    }

    @Test
    public void testListWithSpy() {
        List spyList = Mockito.spy(new ArrayList());

        spyList.add("one");

        Mockito.verify(spyList).add("one");

        assertEquals(1, spyList.size());
    }
}
