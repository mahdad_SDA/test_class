package com.sda.tdd.example.mockito.methods;



import calculator.Calculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorMockitoTest {

    //given
    @Mock
    Calculator calculator;

    @Test
    public void testAddWithPositiveNumbersReturnResult() {
        //when //then
        when(calculator.div(5, 5)).thenReturn(100);
//        System.out.println(calculator.add(3, 5));
        when(calculator.add(3,2)).thenReturn(50);
        System.out.println(calculator.div(0, 0));
        System.out.println(calculator.add(3, 2));
    }

    @Test
    public void testAddWithNegativeNumbersReturnResult() {
        when(calculator.add(-3, -5)).thenReturn(-8);
    }

    @Test
    public void testAddWithAnyNumberReturnResult() {
        when(calculator.add(anyInt(), anyInt())).thenReturn(20);
        System.out.println(calculator.add(3, 2));
        System.out.println(calculator.add(4, 12));
    }
}
