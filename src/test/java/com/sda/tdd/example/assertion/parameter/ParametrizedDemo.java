package com.sda.tdd.example.assertion.parameter;

import calculator.Calculator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParametrizedDemo {

    @Parameterized.Parameters
    public static Collection<Integer[]> parameters() {
        return Arrays.asList(new Integer[][] {
                {1, 1, 2},
                {2, 4, 6},
                {1, 6, 7},
                {4, 1, 5}
        });
    }
    @Parameterized.Parameter(0)
    public int num1;

    @Parameterized.Parameter(1)
    public int num2;

    @Parameterized.Parameter(2)
    public int expectedValue;

    @Test
    public void testAdd_Successful() {
        // given
        Calculator calculator = new Calculator();
        // when
        int result = calculator.add(num1, num2);
        // then
        assertEquals(expectedValue, result);
    }

}
