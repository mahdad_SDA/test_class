package com.sda.tdd.example.assertion.methods;

import calculator.Calculator;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Random;

import static org.junit.Assert.*;

public class MethodsDemo {

    @Test
    public void whenAssertingEquality_thenEqual() {
        String expected = "Test";
        String actual = "Test";
        assertEquals(expected, actual);
    }

    @Test
    public void shouldAssertEqualsPass() {
        assertEquals(64, 32 * 2);
    }

    @Test
    public void whenAssertEqualsAssertionErrorWithComment() {
        assertEquals("Values are not equal", 1, 2);
    }


    @Test
    public void whenAssertingArraysEquality_thenEqual() {
        char[] expected = {'T', 'e', 's', 't'};
        char[] actual = "Test".toCharArray();
        assertArrayEquals(expected, actual);
    }

    @Test
    public void whenAssertArrayEquals_thenEqual() {
        String[] expectedArray = {"one", "two", "three"};
        assertArrayEquals(expectedArray,
                new String[]{"one", "two", "three"});
    }

    @Test
    public void whenAssertNullThenNull() {
        Object object = null;
        assertNull(object);
    }

    @Test
    public void shouldAssertTruePass() {
        assertTrue("".equals(""));
        String test = "Test";
        assertTrue(test.equals("Test"));
    }

    @Test
    public void shouldAssertTrueThrowAssertionError() {
        assertFalse(true == false);
    }


    @Test(expected = NullPointerException.class)
    public void thrownNullPointerException() {
        //given
        Calculator calculator = new Calculator();
        //when
        calculator.addFakeException();
        //then
    }

    @Test(expected = ArithmeticException.class)
    public void divExpectArithmeticException() {
        //given
        Calculator calculator = new Calculator();
        //when
        calculator.div(10, 0);
        //then
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldThrowExceptionWhenDividingBy0() {
        // give
        expectedException.expect(ArithmeticException.class);
        expectedException.expectMessage("/ by zero");
        Calculator calculator = new Calculator();
        // when
        calculator.div(10, 0);
        // then
        // should throw expected exception
    }

    @Test
    public void divReturnSuccess(){
        //given
        Calculator calculator= new Calculator();
        //when
        int  actual = calculator.div(10,0);
        //then
        Assert.assertEquals(2,actual);
    }



}
