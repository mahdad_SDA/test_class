package com.sda.tdd.example.assertion.lifecycle;

import org.junit.*;

import static org.junit.Assert.*;

public class LifeCycleDemo {

    @Before
    public void setUp() {
        System.out.println("Run before each test");
    }
    @After
    public void afterTest(){
        System.out.println("Run after each test");
    }
    @BeforeClass
    public static void setUpBeforeClass(){
        System.out.println("Run before the first test method");
    }
    @AfterClass
    public static void tearDownAfterClass() {
        System.out.println("Run after the last test method");
    }

    @Test
    public void test() {
        assertTrue(true);
    }

    @Test
    public void test2(){
        assertNull(null);
    }

}
