package com.sda.tdd.example.assertion.methods;

import org.junit.Test;
import java.util.Arrays;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class ThatMatchersDemo {

    @Test
    public void assertThat1() {
        int a = 1, b = 1;
        assertThat(a, is(b));
        assertThat(a, equalTo(b));
        assertThat(a, is(equalTo(b)));
    }

    @Test
    public void assertThat2() {
        int a = 1, b = 1, c = 2;
        assertThat(a, anyOf(is(b), is(c)));
    }

    @Test
    public void testAssertThatHasItems() {
        assertThat(Arrays.asList("Java", "Spring", "Hibernate"),
                hasItems("Java", "Hibernate"));
    }

    @Test
    public void testAssertThatAnyOfContainsString() {
        assertThat("test", anyOf(is("testing"),
                containsString("est")));
    }


}
