package com.sda.tdd.test;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.AllOf.allOf;

public class SampleTest {

    @Test
    public void TestIsInt(){
        int a = 1, b = 1;
        assertThat(1, is(b));
    }

    @Test
    public void TestIsString(){
        String a = "Test a", b = "Test a";
        assertThat(a, is(b));
    }

    @Test
    public void TestAnyOf(){
        int b = 1, c = 2;
        assertThat(1, anyOf(is(b), is(c)));
    }

    @Test
    public void TestAllOf(){
        int b = 1, c=1; //c = 2;
        assertThat(1, allOf(is(b), is(c)));
    }



}
