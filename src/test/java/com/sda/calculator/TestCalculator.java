package com.sda.calculator;


import calculator.Calculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCalculator{

    @Test
    public void testAddResultSuccess(){

        //given
        Calculator calculator = new Calculator();

        //when
        int result = calculator.add(10,2);

        //then
        assertEquals(12,result);

    }


    @Test
    public void testPercentageReturn10(){
       Calculator calculator = new Calculator();

       //when
        double result = calculator.calculatePercentage(100,10);

        //then
        assertEquals(10,result,2);

    }

    @Test
    public void testFactorialReturn120(){
        Calculator calculator= new Calculator();

        //when
        int result =  calculator.fact(5);

        //then
        assertEquals(120,result);
    }

    @Test
    public void testPowerOfReturn25(){
       Calculator calculator= new Calculator();

       //
        //when
        int result =  calculator.powerOf(5,2);

        //then
        assertEquals(25,result);


    }




}
