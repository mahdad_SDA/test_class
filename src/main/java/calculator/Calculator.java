package calculator;

public class Calculator {
    public int add(int num1, int num2) {
        return num1 + num2;
    }

    public double calculatePercentage(int num, int percentage) {
        return (num * percentage) / 100;
    }

    public int fact(int num) {
        int result = 1;
        while (num > 0) {
            result *= num;
            num -= 1;
        }
        return result;
    }


    public int powerOf(int num1, int num2) {
        return (int) Math.pow(num1, num2);
    }

    public void addFakeException() {
        throw new NullPointerException();
    }

    public int div(int num1, int num2) {
        try {
            return num1 / num2;
        }catch(ArithmeticException e){
            throw new ArithmeticException();
        }

    }
}
